# PHP Dockerfiles

Build and push several php base images yo be used in my custom projects
This image can run as many bash `entrypoints` files as needed by a developer (see examples)

`yarn` and `less` are installed by default

## Usage
Update `.gitlab-ci.yml` and set `$CI_REGISTRY_IMAGE` for your repo image registry preference or update your config to set these variables

This script walk trough php directory and find every docker and build based and image on version file in the same directory. The last step is to push image to  desired repository

*PS:* if you want to add new language folders, the only major change is in `build.sh` and `push.sh` to find these new directories.

## Supported Versions 

PHP version: 7.2 
Note: you can change this version on Dockerfiles directly

## Pre installed extensions and packages

#### OS
|  Extension | fpm  | cli  | roadrunner  |
| ---|:---:|:---:|:---:|
| curl  | &check;  |  &check; |  &check; |
| yarn  | &check;  |  &check; | &check;  |
| less  | &check;  |  &check; | &check;  |
| gettext  | &check;  |  &check; | &check;  |
| jq  | &check;  |  &check; | &check;  |

#### PHP
As we need this images for our custom projects a set of pre-installed php extensions

|  Extension | fpm  | cli  | roadrunner  |
| ---|:---:|:---:|:---:|
| pdo_mysql  | &check;  |  &check; |  &check; |
| zip  | &check;  |  &check; | &check;  |
| bcmath  |  &check; | &check;  |  &check; |
| soap  | &check;  | &check;  |  &check; |
| intl  |  &check; | &check;  | &check;  |
| calendar  |  &check; |   | &check;  |

Thanks to: We use this [great helper from Michele Locati](https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/) to avoid extra work to install extensions on php official image 

## Examples
Example of use of this images in Dockerfiles can be viewed on [examples](examples) folder

### Dockerfile
```Dockerfile
FROM node:13 as webpack_statics

WORKDIR /app
COPY ./webpack.config.js ./package.json ./
ADD ./assets ./assets

RUN yarn install && yarn build

# php fpm with custom image
FROM registry.gitlab.com/rcastellanosm/dockerfiles/tools/php:7.3-fpm as php

COPY ./composer.json ./composer.lock  ./
RUN composer install --no-dev --no-interaction --no-scripts

COPY . .

# web server instance
FROM nginx:1.17 as web_server

COPY ./etc/nginx/default.conf /etc/nginx/conf.d/default.conf

COPY --from=webpack_statics /app/public/build ./var/www/public/build
```

### Pipelines CI/CD
```yaml
.test: &test
  image: registry.gitlab.com/rcastellanosm/dockerfiles/tools/php:7.2-cli
  stage: tests
  before_script:
    - php -d memory_limit=3G /usr/bin/composer install --no-interaction --no-scripts
```