#!/usr/bin/env bash
set -e

# process multiple entrypoints files inside entrypoint.d directory
for ep in /etc/entrypoint.d/*; do
  ext="${ep##*.}"
  if [ "${ext}" = "sh" ] && [ -x "${ep}" ]; then
    # run scripts ending in ".sh"
    echo "Running: ${ep} $@"
    "${ep}" "$@"
  fi
done

# run command with exec to pass control
exec "$@"