#!/usr/bin/env bash

# recursive walk trough directories and build docker image for every match
# {$f} => directory name
# ${TAG} => ${CI_REGISTRY_REPO}/${NAMESPACE}/${TECHNOLOGY}:${VERSION}-${TAG}

REGISTRY_REPO="${1}"

for f in php/*; do
    if [[ -d "$f" ]] ; then
        VERSION=$(head -n 1 "php/${f##*/}/version")
        TAG="${REGISTRY_REPO}/php:${VERSION}-${f##*/}"

        docker pull ${TAG}
        docker build --cache-from ${TAG} -t ${TAG} -f ${f}/Dockerfile .
    fi
done